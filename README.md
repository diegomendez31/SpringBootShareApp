# ShareBook Web Platform

Share a book, share culture.


## Description

This a RESTful web platform based on: 

- Java
- Gradle
- SpringBoot
- JavaScript
- AngularJS
- MySQL


### Pitch Presentation.

Please find the PDF pitch presentation for this app within the repository. 

For the Android version follow: 

https://gitlab.com/diegomendez31/ShareBookApp




